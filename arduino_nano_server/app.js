const http = require("http");
const PORT = process.env.PORT || 3001;


const { Board, Led } = require("johnny-five");
const board = new Board();

board.on("ready", () => {
    const led = new Led(13);

    board.repl.inject({
        led
    });

    const server = http.createServer(async (req, res) => {

        if (req.url === "/prender" && req.method === "GET") {
            res.writeHead(200, { "Content-Type": "application/json" });
            res.write("Hi there, This is a Vanilla Node.js API");
            res.end();
            led.on();

        }

        else if (req.url === "/apagar" && req.method === "GET") {
            res.writeHead(200, { "Content-Type": "application/json" });
            res.write("Hi there, This is a Vanilla Node.js API 2");
            res.end();
            led.off();
        }
        else {
            res.writeHead(404, { "Content-Type": "application/json" });
            res.end(JSON.stringify({ message: "Route not found" }));
        }
    });

    server.listen(PORT, () => {
        console.log('server started on port: ${PORT}');
    });
});




