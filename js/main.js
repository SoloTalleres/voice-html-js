var artyom = new Artyom();

document.querySelector("#activar").addEventListener('click', function(){
    artyom.say("sonido activado");
});

artyom.addCommands({
    indexes: ["PRENDER","APAGAR", "ROJO"],
    action: function(i){
        if(i == 0){
            artyom.say("PRENDIDO");
            document.querySelector("#content").style.background = "green";
            console.log("Color VERDE");

            var url1 = "http://localhost:3001/prender"
            fetch(url1)
                .then((response) => {
                    let data = response.json();
                    console.log(data)
                    return data
                })

        }else if(i == 1){
            artyom.say("APAGADO");
            document.querySelector("#content").style.background = "yellow";
            console.log("Color AMARILLO");
            var url2 = "http://localhost:3001/apagar"
            fetch(url2)
                .then((response) => {
                    let data = response.json();
                    console.log(data)
                    return data
                })
        }
        else if(i == 2) {
            artyom.say("cambio el color a rojo");
            document.querySelector("#content").style.background = "red";
            console.log("Color ROJO");

        }
    }
});

artyom.initialize({
    lang:"es-ES",
    debug:true,
    listen:true,
    continuous: true,
    speed:0.9,
    mode:"normal"
});

artyom.redirectRecognizedTextOutput(function(recognized,isFinal){
    if(isFinal){
        console.log("Texto final reconocido: " + recognized);
    }else{
        console.log(recognized);
    }
});